.. documentation on the parameters from cookiecutter.json

.. list-table:: Parameters
   :header-rows: 1

   * - Parameter
     - Description
     - Default
     - Format
   * - ``project_authors``
     - Full names of the author(s)
     - ``Firstname Lastname, Another-Firstname Lastname``
     - Full name in the format ``Firstname Lastname``.
       In case of multiple authors, they need to be comma-separated.

       .. important::

          The number of authors must match the number of emails in
          ``project_author_emails``!

   * - ``project_author_emails``
     - Email address of the author(s)
     - ``someone@hereon.de, someone.else@hereon.de``
     - Raw email as ``username@domain.de``.
       In case of multiple authors, they need to be comma-separated.

       .. important::

          The number of emails must match the number of authors in
          ``project_authors``

   * - ``project_maintainers``
     - Full names of the maintainer(s)
     - Fallback to ``project_authors``
     - Same as for ``project_authors``.

       .. important::
          The number of maintainers must match the number of emails in
          ``project_maintainer_emails``!
   * - ``project_maintainer_emails``
     - Email address of the maintainer(s)
     - Fallback to ``project_author_emails``
     - Same as for ``project_authors``.

       .. important::

          The number of emails must match the number of maintainers in
          ``project_maintainer_emails``!
   * - ``gitlab_host``
     - Fully qualified domain of the Gitlab Server.
     - ``codebase.helmholtz.cloud``
     - A valid domain name
   * - ``gitlab_username``
     - The user or group name on GitLab. Can also be the full path to a subgroup
     - ``hcdc``
     - a single slug or multiple slugs separated by ``/``
   * - ``git_remote_protocoll``
     - Whether to register the remote as ``https`` or ``ssh`` after setting up
       the project locally. Does not have an effect on the documentation.
     - ``https``
     - One of ``https`` or ``ssh``
   * - ``institution``
     - Name of the institution that should be acknowledged
     - ``Helmholtz-Zentrum Hereon``
     - string
   * - ``institution_url``
     - Public website of the institution
     - ``https://www.hereon.de``
     - A url
   * - ``copyright_holder``
     - Body that has the copyright on the package.
     - Fallback to ``institution``
     - string
   * - ``copyright_year``
     - Year or year range of the copyright
     - the current year
     - a year (e.g. ``2013``) or a year range (e.g. ``2021-2023``)
   * - ``use_reuse``
     - whether to use the reuse tool or not
     - ``yes``
     - ``yes`` or ``no``
   * - ``code_license``
     - SPDX license identifier for the code (aka python files).

       .. important::

          Please clarify this with the head of your institute! Changing
          licenses afterwards can be very difficult.

     - ``EUPL-1.2``
     - A valid SPDX-identifier, see https://spdx.org/licenses/
   * - ``documentation_license``
     - SPDX license identifier for the documentation.

       .. important::

          Please clarify this with the head of your institute! Changing
          licenses afterwards can be very difficult.

     - ``CC-BY-4.0``
     - A valid SPDX-identifier, see https://spdx.org/licenses/
   * - ``supplementary_files_license``
     - SPDX license identifier for technical files such as ``.gitignore`` or
       python package configuration.

       .. important::

          Please clarify this with the head of your institute! Changing
          licenses afterwards can be very difficult.
     - ``CC0-1.0``
     - A valid SPDX-identifier, see https://spdx.org/licenses/
   * - ``project_title``
     - Short title for the project
     - ``My First Documentation``
     - string, can contain spaces
   * - ``project_slug``
     - Slug of the project on GitLab
     - Fallback to ``project_title`` but with ``-`` instead of spaces
     - string without spaces or slashes (should match the following pattern
       ``^[-a-zA-Z0-9_]+\Z``)
   * - ``project_short_description``
     - Optional description for the package
     - Fallback to project title
     - arbitrary string
   * - ``keywords``
     - keywords for the project
     - *empty string*
     - comma-separated list of strings. Each string should represent one
       keyword
   * - ``documentation_url``
     - URL where the documentation will be hosted
     - the default URL from the HZDR gitlab
     - a valid URL, should end with a trailing ``/``
   * - ``use_rtd_theme``
     - Whether or not to use the sphinx-rtd-theme package. If False, we will use
       the alabaster theme
     - ``yes``
     - ``yes`` or ``no``
   * - ``deploy_pages_in_ci``
     - Whether or not to deploy the documentation with GitLab Pages.
     - ``yes``
     - ``yes`` or ``no``
