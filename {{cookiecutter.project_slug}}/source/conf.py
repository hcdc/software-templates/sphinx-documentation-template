# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------

import yaml

with open("../CITATION.cff") as f:
    citation_info = yaml.safe_load(f)

project = "{{ cookiecutter.project_slug }}"
copyright = "{{ cookiecutter.copyright_year }} {{ cookiecutter.copyright_holder }}"
author = ", ".join(
    "%(given-names)s %(family-names)s" % d for d in citation_info["authors"]
)


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.intersphinx",
    "sphinx_design",
    "sphinx.ext.napoleon",
    "sphinx.ext.todo",
    "myst_parser"
]

myst_enable_extensions = ["fieldlist"]

source_suffix = {
    ".rst": "restructuredtext",
    ".md": "markdown",
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

show_authors = True

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
{% if cookiecutter.use_rtd_theme == "yes" %}
html_theme = "sphinx_rtd_theme"

html_theme_options = {
    "collapse_navigation": False,
    "includehidden": False,
    "style_nav_header_background": "white",
}

html_logo = "_static/hereon_Logo.png"
{% else %}
html_theme = "alabaster"

html_theme_options = {
    "logo": "hereon_Logo.png",
    "show_related": True,
    "show_relbars": True,
}

html_sidebars = {
    "**": [
        "about.html",
        "globaltoc.html",
        "relations.html",
        "sourcelink.html",
        "searchbox.html",
    ],
}
{% endif %}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]


intersphinx_mapping = {
    "python": ("https://docs.python.org/3/", None),
}
