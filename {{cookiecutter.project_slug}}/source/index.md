<!--
{{ cookiecutter.project_slug }} documentation master file
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.
-->

# {{cookiecutter.project_title }}

{% if cookiecutter.project_short_description %}**{{ cookiecutter.project_short_description }}**{% endif %}

```{warning}

This page has been automatically generated as has not yet been reviewed
by the authors of {{ cookiecutter.project_slug }}! Stay tuned for
updates and discuss with us at <https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}>
```

```{toctree}
---
maxdepth: 2
caption: "Contents:"
---
```

## How to cite this document

```{eval-rst}
.. card:: Please do cite this document!

   .. tab-set::

      .. tab-item:: APA

         .. citation-info::
            :format: apalike

      .. tab-item:: BibTex

         .. citation-info::
            :format: bibtex

      .. tab-item:: RIS

         .. citation-info::
            :format: ris

      .. tab-item:: Endnote

         .. citation-info::
            :format: endnote

      .. tab-item:: CFF

         .. citation-info::
            :format: cff
```

# License information

Copyright © {{ cookiecutter.copyright_year }} {{ cookiecutter.copyright_holder }}

{% if cookiecutter.use_reuse == "yes" %}

If not stated otherwise, the contents of this documentation is licensed
under {{ cookiecutter.documentation_license }}.

{% else %}
All rights reserved.
{% endif %}

## Indices and tables

-   {ref}`genindex`
-   {ref}`modindex`
-   {ref}`search`
