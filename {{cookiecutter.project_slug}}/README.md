# {{ cookiecutter.project_title }}

{% if cookiecutter.project_short_description != cookiecutter.project_title %}{{ cookiecutter.project_short_description }}{% endif %}

## Building this documentation

To build and modify this documentation yourself, clone the
[source code][source code] from gitlab and make your changes in the
[source](source) folder.

```bash
git clone https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}
```

You can then build the documentation by first
installing the necessary requirements via

```bash
pip install -r requirements.txt
```

Now start building the docs via

```bash
make html  # or make.bat html on windows
```

and open the file in `builds/html/index.html` with any browser.

[source code]: https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}

## Extensions

This documentation is based upon sphinx and uses the following extension
modules:

- [myst-parser][myst-parser] for rendering markdown
- [sphinx-design][sphinx-design] for tabs, cards, etc.
{%- if cookiecutter.use_rtd_theme == "yes" %}
- [sphinx-rtd-theme][sphinx-rtd-theme] as theme
{%- endif %}


[myst-parser]: https://myst-parser.readthedocs.io
[sphinx-design]: https://sphinx-design.readthedocs.io
{%- if cookiecutter.use_rtd_theme == "yes" %}
[sphinx-rtd-theme]: https://sphinx-rtd-theme.readthedocs.io
{%- endif %}


## Technical note

This package has been generated from the template
{{ cookiecutter._template }}.

See the template repository for instructions on how to update the skeleton for
this package.



## License information

Copyright © {{ cookiecutter.copyright_year }} {{ cookiecutter.copyright_holder }}

{% if cookiecutter.use_reuse == "yes" %}

Licensed under the {{ cookiecutter.code_license }}

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the {{ cookiecutter.code_license }} license for more details.

{% else %}
All rights reserved.
{% endif %}